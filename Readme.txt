A basic interface for communicating with a beckhof system via ADS interface
Suitable for basic display on a PC or Raspberry Pi
Requires using Python3 (which tripped me up getting it going on the raspberry pi)

Built using tkinter for the user interface
https://wiki.python.org/moin/TkInter

pyads for the ADS communications
https://pypi.org/project/pyads/
