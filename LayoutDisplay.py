from __future__ import print_function
import pyads
from tkinter import *
#from PIL import Image,ImageTk
import time



window = Tk()
window.title("LayoutDisplay")
window.geometry('300x200')
#window.attributes("-fullscreen", True)
PLCConnected = False

pyads.open_port()
remote_ip = '192.168.8.121'
ams_id = '5.65.196.54.1.1'
portNum = 851
#linux required section
if pyads.utils.platform_is_linux():
    print('Yay for the penguin')
    adr = pyads.AmsAddr(ams_id, portNum)
    pyads.add_route(adr, remote_ip)
#End Linux Section
plc = pyads.Connection(ams_id, portNum,remote_ip)
plc.open()


lbl = Label(window, text="Waiting To connect")
lbl.grid(column=0, row=0)
lbl.config(font=("Courier", 50))

#canvas = Canvas(window, width = 300, height = 300)
#canvas.grid(column=1, row=1)
#img = ImageTk.PhotoImage(Image.open("ball.png"))
#canvas.create_image(100, 100, anchor=NW, image=img)

def readMotorStatus():
    motorStatus = plc.read_by_name('GVL.EnableMotor', pyads.PLCTYPE_BOOL)
    print(motorStatus)

def clicked():
    print("connecting")
    # Connect to the local TwinCAT PLC
    plc.open()
    PLCConnected = True

def UpdateImage():
    try:
        motorStatus = plc.read_by_name('GVL.TrayLayout', pyads.PLCTYPE_INT)
        lbl.configure(text=('Size: ' + str(motorStatus)))
    except:
        lbl.configure(text="PLC NC")
    window.after(1000, UpdateImage)


#btn = Button(window, text="Connect", command=clicked)
#btn.grid(column=1, row=0)

#ReadMotor = Button(window, text="readVar", command=readMotorStatus)
#ReadMotor.grid(column=2, row=0)
UpdateImage()
window.mainloop()
plc.close()
